window.injectScript = function (text) {
    const parent = document.documentElement,
        script = document.createElement('script');
    script.text = text;
    script.async = false;
    parent.insertBefore(script, parent.firstChild);
};

const noisyToDataURL = `(${function () {
    HTMLCanvasElement.prototype.toDataURL = function (orig) {
        return function () {
            const now = new Date(),
                date = now.toLocaleDateString(),
                time = now.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}),
                stamp = `${date} ${time}`,
                context = this.getContext('2d');
            context.save();
            context.textBaseline = 'top';
            context.fillStyle = 'rgba(255, 255, 255, 0.01)';
            context.fillText(stamp, 0,0, this.width);
            context.restore();
            return orig.apply(this, arguments);
        };
    }(HTMLCanvasElement.prototype.toDataURL);

}}());`;

(function () {
    if (!(document instanceof HTMLDocument) && (!(document instanceof XMLDocument) ||
        !(document.createElement('div') instanceof HTMLDivElement))) {
        return;
    }
    window.injectScript(noisyToDataURL);
}());
